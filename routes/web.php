<?php

use App\Http\Livewire\AccountTypes;
// use App\Models\AccountType;
use App\Models\Unit;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    // return view('layouts.base');
});


Route::get('lang/{locale}', function ($locale) {
    App::setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
});


Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->group(function(){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('users', App\Http\Controllers\UserController::class);

    Route::resource('brands', App\Http\Controllers\BrandController::class);


    Route::middleware('auth')->group(function() {
        // Route::livewire('/path-to-component', 'component');
        Route::view('accounts','accounts.home'); // ហៅ home.blade.php ពីក្នុង views/units
        // Route::view('account_types','livewire.account-types'); // ហៅ home.blade.php ពីក្នុង views/units
        // Route::view('account_types',AccountType::class); // ហៅ home.blade.php ពីក្នុង views/units
        Route::view('account-types','account_types.home'); // ហៅ home.blade.php ពីក្នុង views/units


        // Route::view('units','units.home'); // ហៅ home.blade.php ពីក្នុង views/units
        // Route::view('categories','livewire.categories'); // ហៅ home.blade.php ពីក្នុង Livewire
        // Route::view('categories','categories.home'); // ហៅ home.blade.php ធម្មតាមិនប្រើ Livewire
        Route::resource('categories', App\Http\Controllers\CategoryController::class);
        Route::resource('units', App\Http\Controllers\UnitController::class);


        Route::resource('products', App\Http\Controllers\ProductController::class);
        Route::resource('carts', App\Http\Controllers\CartController::class);



    });





});
