<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //Sidebars
    'Dashboard' =>'Dashboard',
    'Manage Users'     =>'Manage Users',
    'User List'     =>'User List',
    'User Roles'     =>'User Roles',
    'Sell Comission Agents'     =>'Sell Comission Agents',

    'Manage Products'    =>"Manage Products",
    'Product List'      =>  "Product List",
    'Categories'      =>  "Categories",
    'Brands'      =>  "Brands",
    'Units'      =>  "Units",
    'Warranties'      =>  "Warranties",
    'Logout'      =>  "Logout",


    // Form Controls

    'add'  => 'Add New',
    'save'  => 'Save',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'update'    =>'Update',
    'export'    =>'Export',
    'cancel' =>'Cancel',
    'close'    =>'Close',
    'exit'    =>'Exit',
    'logout' =>'Logout',
    'save close' =>'Save Close',
    'created by' =>'Created by',
    'action' =>'Action',
    'Your data has been saved or updated!' =>'Your data has been saved or updated!',

    'delete_message_can_not'    =>'You can\'t delete this!',
    'Your file has been deleted.'    =>'Your item has been deleted.',
    'Are you sure?' =>'Are you sure?',
    'Yes, delete it!' =>'Yes, delete it!',
    'You will not be able to delete this!' =>'You will not be able to delete this!',

    // Brands Form
    'all_brands'    =>'List all brands',
    'b_name'    =>'Brand Name',
    'description' =>'Description',

     // Category Form
     'Categories'    =>'Categories',
     'Category Name'    =>'Category Name',
     'Parent Category'    =>'Parent Category',
     'Description' =>'Description',

    // Products
    'Products'  => 'Products',


      // Units
      'all_units' =>  'ឯកតា​ទាំងអស់',
      'Unit Name' =>  'ឈ្មោះឯកតា',
      'Short Name' =>  'ឈ្មោះឯកតាខ្លី',




    // Accounts
    'Accounts'    =>'Accounts',
    'id'    =>'No',
    'account no'    =>'Account No',
    'account name'    =>'Account Name',
    'account type'    =>'Account Type',
    'account note'    =>'Note',
    'account is closed'    =>'is Closed',


    'create account'    =>'Create Account',
    'Select Option'     =>'Please Select',
    'Yes'               =>'Yes',
    'No'                =>'No',
    'Created By'        =>'Created by',
    'balance'           =>'Balance',
    'amount'           =>'Amount',



];
