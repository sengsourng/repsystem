<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'list_all_users' => 'បញ្ចីអ្នក​ប្រើប្រាស់​ទាំងអស់',
    'new_user' =>'អ្នក​ប្រើប្រាស់​ថ្មី',
    'active_user' =>'អ្នក​កំពុងប្រើ​',
    'role'  =>'សិទ្ធិប្រើប្រាស់',
    'all_users' =>'អ្នក​ប្រើទាំងអស់',
    'add_new' =>'បន្ថែមថ្មី',

];
