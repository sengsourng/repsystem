<?php

return [

    //Sidebars
    'Dashboard'         =>'ទំព័រមុខ',
    'Manage Users'      =>'គ្រប់គ្រងអ្នកប្រើ',
    'User List'         =>'បញ្ចីអ្នកប្រើ',
    'User Roles'        =>'សិទ្ធិអ្នកប្រើ',
    'Sell Comission Agents'     =>'កំរៃជើងសារភ្នាក់ងាលក់',

    'Manage Products'    =>"គ្រប់គ្រងផលិតផល",
    'Product List'      =>  "បញ្ចីផលិតផល",
    'Categories'      =>  "ក្រុមទំនិញ",
    'Brands'      =>  "ម៉ាកទំនិញ",
    'Units'      =>  "ឯកតាទំនិញ",
    'Warranties'      =>  "ធានារ៉ាប់រង",
    'Logout'      =>  "ចាកចេញ",


    // Form Controls

    'add'  => 'បន្ថែមថ្មី',
    'save'  => 'រក្សាទុក',
    'edit' => 'កែប្រែ',
    'delete' => 'លុប',
    'update'    =>'ធ្វើបច្ចុប្បន្នភាព',
    'export'    =>'នាំចេញ',
    'close'    =>'បិទ',
    'exit'    =>'ចាកចេញ',
    'cancel' =>'បោះបង់',
    'logout' =>'ចាកចេញ',
    'save close' =>'រក្សាទុក និងបិទ',
    'created by' =>'បង្កើតដោយ',
    'action' =>'សកម្មភាព',
    'Your data has been saved or updated!' =>'ទិន្នន័យរបស់អ្នកត្រូវបានរក្សាទុកឬធ្វើបច្ចុប្បន្នភាព!',
    'delete_message_can_not'    =>'អ្នកមិនអាចលុប ឡើយ',
    'Your file has been deleted.'    =>'អ្នកបានលុបដោយជោគជ័យ។',
    'Are you sure?' =>'តើអ្នកប្រាកដទេ?',
    'Yes, delete it!' =>'បាទ, ខ្ញុំលុបវា!',
    'You will not be able to delete this!' =>'អ្នកនឹងមិនអាចលុបវា !',

    // Brands Form
    'all_brands'    =>'បញ្ចី​ម៉ាកទាំងអស់',
    'b_name'    =>'ឈ្មោះម៉ាក',
    'description' =>'អធិប្បាយពន្យល់',

     // Category Form
     'Categories'    =>'បញ្ចីក្រុមទំនិញ',
     'Category Name'    =>'ឈ្មោះក្រុមទំនិញ',
     'Parent Category'    =>'មេក្រុមទំនិញ',
     'Description' =>'អធិប្បាយពន្យល់',


    // Products
    'Products'  => 'ផលិតផល',


    // Units
    'all_units' =>  'ឯកតា​ទាំងអស់',
    'Unit Name' =>  'ឈ្មោះឯកតា',
    'Short Name' =>  'ឈ្មោះឯកតាខ្លី',


    // Accounts
    'Accounts'    =>'បញ្ចីគណនី',
    'id'    =>'ល.រ',
    'account no'    =>'លេខគណនី',
    'account name'    =>'ឈ្មោះ​គណនី',
    'account type'    =>'ប្រភេទ​គណនី',
    'account note'    =>'កត់ចំណាំ​គណនី',
    'account is closed'    =>'គឺបានផ្គាក',


    'create account'    =>'បង្កើតគណនី',
    'Select Option'     =>'សូមជ្រើសរើស',
    'Yes'               =>'បាទ/ចាស',
    'No'                =>'ទេ',
    'Created By'        =>'បង្កើតដោយ',
    'balance'           =>'សមតុល្យ',
    'amount'           =>'ចំនួន',




];
