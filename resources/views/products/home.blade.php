@extends('layouts.admin')

@push('css')

@endpush

@push('title','Manage Products')

@section('content-header',"Manage Products")


@section('content')

     <!--Section: Sales Performance KPIs-->
     <section class="mb-4">
        <div class="card">
          <div class="card-header text-left py-3" style="height: 9vh;">
            <h5 class="mb-0 text-left">

                <strong>All Products</strong>

                <span >
                    <a href="{{route('products.create')}}"
                    class="btn btn-primary btn-sm float-right"

                    data-toggle="modal-ajax"
                    {{-- data-toggle="modal" --}}
                    data-modal-size="modal-lg"
                    {{-- data-mdb-toggle="modal" --}}
                    {{-- data-target="#exampleModal" --}}
                    data-target="#openModal"
                    >
                    <i class="fas fa-plus-circle"></i> {{trans('controls.add')}}</a>
                </span>
            </h5>
          </div>


          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover text-nowrap" id="DataTableList">
                <thead>
                  <tr>
                    <th scope="col" style="width: 40px !important;">ID</th>
                    <th scope="col">{{__('Name')}}</th>
                    <th scope="col">{{__('controls.Categories')}}</th>
                    <th scope="col">{{__('controls.Brands')}}</th>
                    <th scope="col">{{__('controls.Units')}}</th>

                    {{-- <th scope="col">{{__('controls.created by')}}</th> --}}
                    <th scope="col">{{__('controls.action')}}</th>
                  </tr>
                </thead>
                <tbody>

                    @foreach ($products as $row)
                        @include('products.rows')
                    @endforeach


                </tbody>
              </table>
            </div>
          </div>
        </div>
     </section>
      <!--Section: Sales Performance KPIs-->

{{-- @endif --}}


@endsection


@push('js')


<script>

    $(document).ready(function() {
        $('#DataTableList').DataTable({
            ordering:true,
            processing: false,
            // serverSide: true,
            "order": [[ 4, "asc" ]],
            language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
        });
    } );

        $(document).on('click',`[data-toggle="modal-ajax"]`,function(ev){
                    ev.preventDefault();
                    var url = $(this).attr('href');
                    var modalSize = $(this).data('modal-size');
                    var title = $(this).text();
                    // var target = $(this).data('mdb-target');
                    var target = $(this).data('target');


                    $.get(url).done((res)=>{
                        var $modal = $(target);
                        $modal.find('.modal-title').text(title);
                        $modal.find('.modal-dialog').addClass(modalSize);
                        $modal.find('.modal-body').html(res);


                        $modal.modal('show');

                        $modal.find('button#btn-submit').unbind().click(function(ev){
                            ev.preventDefault();

                            var $form = $modal.find('form');
                            var formData = new FormData($form[0]);
                            $.ajax({
                                url : $form.attr('action'),
                                method : 'post',
                                data : formData,
                                processData : false,
                                contentType : false,
                                success : (res)=>{
                                    if(res.status){

                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '{{__('controls.Your data has been saved or updated!')}}',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });


                                        if($(this).data('allow-close')){
                                            $modal.find(`[data-dismiss="modal"]`).click();
                                        }
                                        if ( url.match('edit')) {
                                            $('#DataTableList').find(`tr[data-id="${res.data.id}"]`).html($(res.dom).html());
                                        }else{
                                            $('#DataTableList').find('tbody').prepend(res.dom);
                                        }

                                        $form[0].reset();
                                    }

                                },
                            })

                        });
                        $modal.find(`[data-dismiss="modal"]`).click(function(ev){
                            ev.preventDefault();
                            $modal.modal('hide');
                            $modal.find('.modal-body').html("");

                        });


                    });

                });

</script>

{{-- Ajax Delete --}}
<script type="text/javascript">
    $(document).on('submit',`[data-toggle="formDelete"]`,function(ev){
        ev.preventDefault();
        $.ajax({
            url : $(this).attr('action'),
            method : 'post',
            data : new FormData(ev.target),
            processData : false,
            contentType : false,
            success : (res)=>{
                if(res.status){
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.Your file has been deleted.')}}',
                        'success'
                    );
                    $(this).parents("tr").remove();
                }else{
                    Swal.fire(
                        '{{trans('controls.delete')}}!',
                        '{{trans('controls.delete_message_can_not')}}',
                        'error'
                    )
                }
            }

        })
    });

    function deleteItem(id){
      Swal.fire({
        title: "{{trans('controls.Are you sure?')}}",
        text: "{{trans('controls.You will not be able to delete this!')}}",
        icon: 'warning',
        showCancelButton: true,

        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText :"{{trans('controls.cancel')}}",
        confirmButtonText:  "{{trans('controls.Yes, delete it!')}}"
      }).then((result) => {
        if (result.value) {
          $('#frmDeleteItem-'+id).submit();

        }
      })
    }
</script>

@endpush
