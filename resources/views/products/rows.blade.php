<tr data-id="{{$row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
    <td>{{$row->name}}<br>
        <span class="text-success">
            {{-- <i class="fas fa-calculator me-1"></i> <span> {{ $row->products()->count() }} {{trans('controls.Products')}}</span> --}}
        </span>
    </td>
    {{-- <td>
        {!! $row->product_description !!}
    </td> --}}
    <td>
        @if ($row->category_id !=null)
            {{ $row->product_cat($row->category_id)->name}}
         @endif
    </td>

    <td>
      @if ($row->brand_id !=null)
        {{ $row->brands($row->brand_id)->name}}
      @endif

    </td>
    <td>
        @if ($row->unit_id !=null)
            {{ $row->product_unit($row->unit_id)->actual_name}}
        @endif
        {{-- {{ $row->unit_id}} --}}
    </td>

    <td>
        <a class="btn btn-primary  edit_button sm" href="{{route('products.edit',$row)}}"
                data-toggle="modal-ajax"
                data-modal-size="modal-lg"
                data-target="#openModal"
        ><i class="fa fa-edit left"></i> {{ trans('controls.edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('products.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
