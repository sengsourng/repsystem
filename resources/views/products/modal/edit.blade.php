<div class="container pt-3 px-4">
    <form action="{{ route('products.update',$product) }}" method="post">
        @csrf
        {{ method_field('PUT') }}
        <div class="row">
            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    {{-- <label class="form-label" for="cat_name">Category Name</label> --}}
                    <input type="text" id="cat_name" class="form-control" name="name" value="{{$product->name}}" placeholder="{{trans('controls.Category Name')}}"/>

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    {{-- <label class="form-label" for="short_code">Short Code</label> --}}
                    <input type="text" id="short_code" class="form-control" name="short_code" value="{{$product->short_code}}" placeholder="Short Code" />

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="parent_id">{{trans('controls.Parent Category')}}</label>
                    <select class="form-control" name="parent_id" id="parent_id">
                        <option value="0">Please Select</option>
                        @foreach ($Parents as $item)
                            <option {{$item->id==$product->parent_id?'selected':''}} value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="description">{{ __('controls.description')}}</label>
                    <textarea id="description" class="form-control" cols="30"  rows="5" name="description">{{$product->description}}</textarea>

                </div>
            </div>
        </div>



    </form>
</div>
