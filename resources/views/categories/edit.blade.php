<div class="container pt-3 px-4">
    <form action="{{ route('brands.store') }}" method="post">
        @csrf

        <div class="row">
            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <input type="text" id="b_name" class="form-control" name="name" />
                    <label class="form-label" for="b_name">{{trans('controls.b_name')}}</label>
                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <textarea id="description" class="form-control" cols="30"  rows="5" name="description"></textarea>
                    {{-- {{!! Form::textarea($name, $value, [$options]) !!}} --}}
                    {{-- <input type="texteara" id="description" class="form-control" name="description" /> --}}

                    <label class="form-label" for="description">{{ __('controls.description')}}</label>
                </div>
            </div>
        </div>



    </form>
</div>
