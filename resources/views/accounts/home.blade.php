@extends('layouts.admin_livewire')

@push('css')

@endpush
@section('content')
  {{-- <livewire:units> --}}
    @livewire('accounts')
@endsection

@push('js')
    <script>
        $(document).ready(function() {
        $('#DataTableList').DataTable({
                language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
            });
        } );


    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4',
        dropdownParent: $('#createModal')
    });

     //Initialize Select2 Elements
     $('.select2bs4').select2({
        theme: 'bootstrap4',
        dropdownParent: $('#editModal')
    });


    </script>

@endpush
