<!-- Modal -->
<div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">{{__('controls.create account')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="accountName">{{__('controls.account name')}}</label>
                    <input type="text" class="form-control" id="accountName"
                    placeholder="{{__('controls.account name')}}" wire:model="name">
                    @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="accountNo">{{__('controls.account no')}}</label>
                    <input type="text" class="form-control" id="accountNo" wire:model="account_number" placeholder="{{__('controls.account no')}}">
                    @error('account_number') <span class="text-danger">{{ $message }}</span>@enderror
                </div>


                <div class="form-group">
                    <label for="account_type_id">{{__('controls.account type')}}</label>
                    {{-- <input type="text" class="form-control" id="exampleFormControlInput3" placeholder="{{__('controls.account type')}}" wire:model="account_type_id" value="1"> --}}

                    <select  id="account_type_id" class="form-control select2bs4 select2 select2-accessible" style="width: 100%; height: 45px !important;" data-select2-id="25" tabindex="-1" aria-hidden="true" wire:model="account_type_id">
                        <option>សូមជ្រើសរើស</option>

                            @foreach ($account_types as $acct)
                                {{-- <optgroup label="{{$acct->parent_account_type_id !=null?$acct->name:1}}"> --}}
                                    <option value="{{$acct->id}}">{{$acct->name}}</option>
                                {{-- </optgroup> --}}
                            @endforeach

                    </select>

                    @error('account_type_id') <span class="text-danger">{{ $message }}</span>@enderror
                </div>

            </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('controls.cancel')}}</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal" data-toggle="formSave">{{__('controls.update')}}</button>
            </div>
        </div>
    </div>
</div>
