@extends('layouts.pos')
@push('css')
<style>
    #table-wrapper {
  position:relative;
}
#table-scroll {
  height:330px;
  overflow:auto;
  margin-top:20px;
}
#table-wrapper table {
  width:100%;

}
#table-wrapper table * {
  /* background:yellow; */
  color:black;
}
#table-wrapper table thead th .text {
  position:absolute;
  top:-20px;
  z-index:2;
  height:20px;
  width:35%;
  border:1px solid red;
}
.product-item{
    float: left;
    padding 5px;
    margin-left:10px;


    /* background-color:#C0C0C0; */
}
img{
    border-radius:5px;
}
.item-title{
    font-size: 12px;
}
</style>
@endpush
@section('content')
    <div id="cart">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                      {{-- <h3 class="card-title">Condensed Full Width Table</h3> --}}
                      <div class="row">
                        <div class="col-md 6">
                            <input type="text" class="form-control" placeholder="Barcode">
                        </div>
                        <div class="col-md 6">
                            <input type="text" class="form-control" placeholder="Customer">

                        </div>
                    </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div id="table-wrapper">
                            <div id="table-scroll">
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th style="width: 50%;">Items</th>
                                        <th>Qty</th>
                                        <th style="width: 40px">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @for ($i = 1; $i < 10; $i++)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>Update software</td>
                                            <td>
                                                <div class="input-group input-group-sm" style="width: 60%;">
                                                    <input type="number" class="form-control" min="1">
                                                    <span class="input-group-append">
                                                      <button type="button" class="btn btn-danger btn-flat "><i class="fas fa-trash-alt text-white"></i></button>
                                                    </span>
                                                  </div>
                                            </td>
                                            <td><span class="badge bg-success">1.00</span></td>
                                        </tr>
                                        @endfor

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col">Total Item:</div>
                            <div class="col text-right">5</div>
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="row">
                            <div class="col"><b>Total Payable:</b></div>
                            <div class="col text-right"><span id="total_payable" class="text-success lead text-bold">$10.00</span></div>
                            <input type="hidden" name="final_total" id="final_total_input" value="0.00">

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                      {{-- <h3 class="card-title">Condensed Full Width Table</h3> --}}
                      <div class="row">
                        <div class="col-md 12">
                            <input type="text" class="form-control" placeholder="Searching...">
                        </div>
                    </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div id="table-wrapper">
                            <div id="table-scroll">

                                @for ($i = 1; $i < 30; $i++)
                                <div class="product-item">
                                    <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" />
                                    <br><span class="item-title">{{$i}} Hot Coffee</span>
                                </div>
                                @endfor


                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>


            </div>


        </div>

<div class="pt-4"></div>



    </div>

@endsection
