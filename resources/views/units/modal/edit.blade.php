<div class="container pt-3 px-4">
    <form action="{{ route('units.update',$unit) }}" method="post">
        @csrf
        {{ method_field('PUT') }}
        <div class="row">
            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    {{-- <label class="form-label" for="cat_name">Category Name</label> --}}
                    <input type="text" id="actual_name" class="form-control" name="actual_name" value="{{$unit->actual_name}}" placeholder="{{trans('controls.Category Name')}}"/>

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    {{-- <label class="form-label" for="short_code">Short Code</label> --}}
                    <input type="text" id="short_name" class="form-control" name="short_name" value="{{$unit->short_name}}" placeholder="Short Code" />

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="base_unit_id">{{trans('Base Unit')}}</label>
                    <select class="form-control" name="base_unit_id" id="base_unit_id">
                        <option value="null">Please Select</option>
                        @foreach ($Parents as $item)
                            <option {{$item->id==$unit->base_unit_id?'selected':''}} value="{{$item->id}}">{{$item->actual_name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="description">{{ __('controls.description')}}</label>
                    <textarea id="description" class="form-control" cols="30"  rows="5" name="description">{{$unit->short_name}}</textarea>

                </div>
            </div>
        </div>



    </form>
</div>
