<tr data-id="{{$row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
    <td>{{$row->actual_name}}<br>
        <span class="text-success">
            {{-- <i class="fas fa-calculator me-1"></i> <span> {{ $row->products()->count() }} {{trans('controls.Products')}}</span> --}}
        </span>
    </td>
    <td>
        {{$row->short_name}}
    </td>
    <td>
        {{-- @if ($row->parent_id !=0)
            {{$row->CatParent($row->parent_id)->name}}
        @endif --}}

    </td>

    <td>
        <a class="btn btn-primary  edit_button sm" href="{{route('units.edit',$row->id)}}"
                data-toggle="modal-ajax"
                data-modal-size="modal-lg"
                data-target="#openModal"
        ><i class="fa fa-edit left"></i> {{ trans('controls.edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('units.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
