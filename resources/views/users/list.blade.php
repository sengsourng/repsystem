<table id="DataTableList" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th class="text-center">ID</th>
      <th>User Name</th>
      <th>Contact</th>
      <th>Company</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($users as $item)
        <tr>
            <td class="text-center">{{$item->id}}</td>
            <td>{{$item->first_name .' '. $item->last_name}}<br>
                <span class="badge badge-success">
                    {{$item->username}}
                </span>
            </td>
            <td>
                {{$item->email}}<br>
                <span class="badge badge-success">
                    {{$item->user_type}}
                </span>

            </td>
            <td>  {{$item->business_id}}</td>
            <td>
                {{-- <button type="button" class="btn  btn-danger btn-flat">Edit</button>
                <button type="button" class="btn  btn-danger btn-flat">Delete</button> --}}

                <div class="btn-group">
                    <button type="button" class="btn btn-success">Action</button>
                    <button type="button" class="btn btn-success dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" role="menu" style="">
                      <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View</a>
                      <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Edit</a>
                      <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">User History</a>
                    </div>
                  </div>
            </td>
          </tr>
        @endforeach

    </tbody>
    <tfoot>
    <tr>
        <th class="text-center">ID</th>
        <th>User Name</th>
        <th>Contact</th>
        <th>Company</th>
        <th>Action</th>
    </tr>
    </tfoot>
  </table>
