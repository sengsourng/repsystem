<section class="mb-4">
    <div class="card">
      <div class="card-header text-left py-3" style="height: 9vh;">
        <h5 class="mb-0 text-left">

            <strong>{{trans('controls.all_units')}}</strong>

            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                <i class="fas fa-plus-circle"></i> {{trans('controls.add')}}
            </button>
        </h5>
      </div>


      <div class="card-body">
        <div class="table-responsive">
            @include('units.update')
            @include('units.create')
            {{-- @if($updateMode)
                @include('units.update')
            @else
                @include('units.create')
            @endif --}}

          <table class="table table-hover text-nowrap" id="DataTableList">
            <thead>
              <tr>
                <th scope="col" style="width: 40px !important;">ID</th>
                <th scope="col">{{__('controls.b_name')}}</th>
                <th scope="col">{{__('controls.description')}}</th>
                {{-- <th scope="col">Gender</th> --}}
                <th scope="col">{{__('controls.created by')}}</th>
                <th scope="col">{{__('controls.action')}}</th>
              </tr>
            </thead>
            <tbody>

                @foreach($units as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->actual_name }}</td>
                    <td>{{ $value->short_name }}</td>
                    <td>{{$value->users($value->created_by)->last_name}}</td>
                    <td>
                        <button wire:click="edit({{ $value->id }})" data-toggle="modal" data-target="#editModal" class="btn btn-primary btn-sm">Edit</button>
                        <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>
                @endforeach


            </tbody>
          </table>
        </div>
      </div>
    </div>
 </section>








 <script type="text/javascript">


</script>
