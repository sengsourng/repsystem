<section class="mb-4">
    <div class="card">
      <div class="card-header text-left py-3" style="height: 9vh;">
        <h5 class="mb-0 text-left">

            <strong>{{trans('controls.Accounts')}}</strong>

            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus-circle"></i> {{trans('controls.add')}}
            </button>
        </h5>
      </div>

      <div class="card-body">
        <div class="table-responsive">
            @include('accounts.update')
            @include('accounts.create')

          <table class="table table-hover text-nowrap" id="DataTableList">
            <thead>
              <tr>
                <th scope="col" style="width: 40px !important;">{{__('controls.id')}}</th>
                <th scope="col">{{__('controls.account no')}}</th>
                <th scope="col">{{__('controls.account name')}}</th>
                {{-- <th scope="col">Gender</th> --}}
                <th scope="col">{{__('controls.account type')}}</th>
                <th scope="col">{{__('controls.balance')}}</th>
                <th scope="col">{{__('controls.Created By')}}</th>
                <th scope="col">{{__('controls.action')}}</th>
              </tr>
            </thead>
            <tbody>

                @foreach($accounts as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->account_number }}</td>
                    <td>
                        {{ $value->name }}


                    </td>
                    <td>
                        {{$value->account_type_id}}
                        {{-- {{$value->AccountTypes($value->account_type_id)->name}} --}}


                    </td>
                    <td>
                        {{ $value->SumAccount('debit',$value->id)->sum('amount')}}
                    </td>
                    <td>
                        <i class="fas fa-user me-1"></i><span>
                            <span class="text-info">
                              {{$value->users($value->created_by)->last_name}}
                            </span> |

                        <span class="text-success">
                            <i class="fas fa-clock me-1"></i><span> {{ session()->get('locale')=='kh'?KhmerDateTime\KhmerDateTime::parse($value->created_at)->fromNow():$value->created_at->diffForHumans() }}</span>
                        </span>

                    </td>
                    <td>
                        <button wire:click="edit({{ $value->id }})" data-toggle="modal" data-target="#editModal" class="btn btn-primary btn-sm editMode"><i class="fa fa-edit"></i> {{__('controls.edit')}}</button>
                        <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> {{__('controls.delete')}}</button>
                    </td>
                </tr>
                @endforeach


            </tbody>
          </table>
        </div>
      </div>
    </div>
 </section>








 <script type="text/javascript">


</script>
