@extends('layouts.admin_livewire')

    @livewireStyles
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/1.9.2/tailwind.min.css" integrity="sha512-l7qZAq1JcXdHei6h2z8h8sMe3NbMrmowhOl+QkP3UhifPpCW2MC4M0i26Y8wYpbz1xD9t61MLT9L1N773dzlOA==" crossorigin="anonymous" />

    <style>
        .rounded-lg, .rounded-b-none
        {
            /* width: 1140px; */
            width: auto;
        }

        .form-input
        {
            width: 450px;
            height: 30px;
        }
    </style>
@section('content')
<div class="container">

    <div class="card">
      <div class="card-header">
        Category
      </div>
      <div class="card-body">
        <livewire:categories
            searchable="id,name, short_code,created_by"
            exportable
         />

      </div>
    </div>



</div>
@endsection

