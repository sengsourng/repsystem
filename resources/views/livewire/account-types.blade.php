<section class="mb-4">
    <div class="card">
      <div class="card-header text-left py-3" style="height: 9vh;">
        <h5 class="mb-0 text-left">
            <strong>{{trans('controls.Accounts')}}</strong>
            <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#createModal">
                <i class="fas fa-plus-circle"></i> {{trans('controls.add')}}
            </button>
        </h5>
      </div>
      <div class="card-body">
        <div class="table-responsive">
            @include('account_types.update')
            @include('account_types.create')

          <table class="table table-hover text-nowrap" id="DataTableList">
            <thead>
              <tr>
                <th scope="col" style="width: 40px !important;">{{__('controls.id')}}</th>
                <th scope="col">{{__('controls.account type')}}</th>
                <th scope="col">{{__('controls.action')}}</th>
              </tr>
            </thead>
            <tbody>
                @foreach($Account_Types as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td>
                            <button wire:click="edit({{ $value->id }})" data-toggle="modal" data-target="#editModal" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> {{__('controls.edit')}}</button>
                            <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> {{__('controls.delete')}}</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
 </section>

