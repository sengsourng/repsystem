@extends('layouts.admin_livewire')

@push('css')

@endpush
@section('content')
  {{-- <livewire:units> --}}
      {{-- connect to file : views->livewire->account-types.blade.php --}}
    @livewire('account-types')

@endsection

@push('js')
    <script>
        $(document).ready(function() {
        $('#DataTableList').DataTable({
                language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
            });
        } );


    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4',
        dropdownParent: $('#createModal')
    });

    </script>


<script type="text/javascript">
    window.livewire.on('userStore', () => {
        $('#createModal').modal('hide');

        $('#editModal').modal('hide');
        // $('.toast').toast('show');


        Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: "{{__('controls.Your data has been saved or updated!')}}",
        // title: "{{session('message')}}",
        showConfirmButton: false,
        timer: 1500
        });
    });

</script>
@endpush
