<!-- Modal -->
<div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">{{__('controls.create account')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="exampleFormControlInput1">{{__('controls.account type')}}</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Name" wire:model="name">
                    @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="parent_account_type_id">Account Parent</label>
                    {{-- <input type="text" class="form-control" id="exampleFormControlInput4" placeholder="Allow decimal" wire:model="parent_account_type_id"> --}}

                    <select name="parent_account_type_id" id="parent_account_type_id" class="form-control" wire:model="parent_account_type_id">
                        <option value="NULL">{{__('controls.Select Option')}}</option>
                        @foreach ($Account_Types as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('parent_account_type_id') <span class="text-danger">{{ $message }}</span>@enderror
                </div>


            </form>

            </div>
            <div class="modal-footer">
                <button type="button"  wire:click.prevent="cancel()" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('controls.cancel')}}</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary close-modal" data-toggle="formSave">{{__('controls.update')}}</button>
            </div>
        </div>
    </div>
</div>
