@extends('layouts.admin_livewire')
@section('content')
  {{-- <livewire:units> --}}
    @livewire('units')
@endsection

@push('js')
    <script>
        $(document).ready(function() {
        $('#DataTableList').DataTable({
                language : $('html').attr('lang') != 'en' ? window.datatableI18n[$('html').attr('lang')] : null,
            });
        } );
    </script>
@endpush
