
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Update Unit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
           <div class="modal-body">
            <form>

                <input type="hidden" wire:model="unit_id">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Name" wire:model="actual_name" value="{{old('actual_name')}}">
                    @error('actual_name') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput2">Short Name</label>
                    <input type="text" class="form-control" id="exampleFormControlInput2" wire:model="short_name" placeholder="Enter short name">
                    @error('short_name') <span class="text-danger">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput3">Business ID</label>
                    <input type="text" class="form-control" id="exampleFormControlInput3" placeholder="Business ID" wire:model="business_id" value="1">
                    @error('business_id') <span class="text-danger">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="allow_decimal">Allow Decimal</label>
                    {{-- <input type="text" class="form-control" id="exampleFormControlInput4" placeholder="Allow decimal" wire:model="allow_decimal"> --}}

                    <select name="allow_decimal" id="allow_decimal" class="form-control" wire:model="allow_decimal">
                        <option value="0">Select Option</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>

                    @error('allow_decimal') <span class="text-danger">{{ $message }}</span>@enderror
                </div>


                {{-- <button wire:click.prevent="store()" class="btn btn-success">Save</button> --}}


                <div class="modal-footer">
                    <button wire:click.prevent="update()" class="btn btn-dark close-modal" data-dismiss="modal">{{__('controls.update')}}</button>
                    {{-- <button wire:click.prevent="cancel()" class="btn btn-danger">Cancel</button> --}}

                    <button type="button" class="btn btn-danger close-btn" data-dismiss="modal">{{__('controls.cancel')}}</button>

                </div>
            </form>

            </div>

        </div>
    </div>
</div>
