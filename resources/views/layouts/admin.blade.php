<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@stack('title','Home Page')</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{asset('js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- Data Table --}}

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">


    <link rel="stylesheet" href="{{asset('themes/sweetalert2/sweetalert2.css')}}">

    <!-- flag-icon-css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">

<style>
    body{
        font-family: 'Hanuman' !important;
        font-size: 1em;
        overflow-y: scroll !important;
    }
    a:visited {
        text-decoration: none !important;
    }
    h3{
        text-decoration: none !important;
    }
</style>

{{-- Using Livewire --}}
@livewireStyles

@stack('css')

</head>
{{-- <body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"> --}}
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">


<div id="app" class="wrapper">

  <!-- Preloader -->
   <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="{{asset('images/logo.png')}}" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
    @include('layouts.partials.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link">
      <img src="{{asset('images/logo.png')}}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    @include('layouts.partials.sidebar')
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    {{-- <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">@yield('content-header')</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header --> --}}

    <!-- Main content -->
    <section class="content pt-4">
        <div class="container-fluid">
             @yield('content')
             {{-- @livewire('content') --}}
        </div> <!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  @include('layouts.partials.control_sidebar')
  <!-- /.control-sidebar -->


  <div class="modal fade"
    id="openModal"
    {{-- tabindex="-1"
    aria-labelledby="openModalLabel"
    aria-hidden="true" --}}
     >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          {{-- <h4 class="modal-title">Large Modal</h4> --}}
          <h5 class="modal-title" id="openModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         {{-- @include('brands.modal.add') --}}
        </div>
        <div class="modal-footer justify-content-right">
          <button type="button" class="btn btn-danger" data-dismiss="modal"> {{__('controls.cancel')}}</button>
          <button type="button" id="btn-submit" class="btn btn-primary" data-allow-close="true">{{__('controls.save close')}}</button>
          <button type="button" id="btn-submit" class="btn btn-success">{{__('controls.save')}}</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->






  <!-- Main Footer -->
  @include('layouts.partials.footer')
</div>
<!-- ./wrapper -->

@livewireScripts



<script src="{{ asset('themes/js/script_translate.js')}}"></script>

  {{-- Data Table --}}

{{-- <script src="{{ asset('https://code.jquery.com/jquery-3.5.1.js')}}"></script> --}}
<script src="{{ asset('https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js')}}"></script>

<script type="text/javascript" src="{{asset('themes/sweetalert2/sweetalert2.min.js')}}"></script>


  @stack('js')


</body>
</html>
