<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@stack('title','Home Page')</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{asset('js/app.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- Data Table --}}

    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('themes/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    {{-- <script src="{{ asset('themes/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}"></script>
    <script src="{{ asset('themes/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"></script> --}}

  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('themes/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('themes/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('themes/sweetalert2/sweetalert2.css')}}">

  <!-- flag-icon-css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">

<style>
    body{
        font-family: 'Hanuman' !important;
        font-size: 1em;
        overflow-y: scroll !important;
    }
    a:visited {
        text-decoration: none !important;
    }
    h3{
        text-decoration: none !important;
    }
</style>

{{-- Using Livewire --}}
@livewireStyles

@stack('css')

</head>
{{-- <body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"> --}}
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">


<div id="app" class="wrapper">

  <!-- Preloader -->
   <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="{{asset('images/logo.png')}}" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
    @include('layouts.partials.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link">
      <img src="{{asset('images/logo.png')}}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    @include('layouts.partials.sidebar')
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">@yield('content-header')</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
             @yield('content')
             {{-- {{$slot}} --}}
             {{-- @livewire('content') --}}
        </div> <!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  @include('layouts.partials.control_sidebar')
  <!-- /.control-sidebar -->




  <!-- Main Footer -->
  @include('layouts.partials.footer')
</div>
<!-- ./wrapper -->

@livewireScripts



<script src="{{ asset('themes/js/script_translate.js')}}"></script>

  {{-- Data Table --}}

{{-- <script src="{{ asset('https://code.jquery.com/jquery-3.5.1.js')}}"></script> --}}
<script src="{{ asset('https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js')}}"></script>
{{--
<script src="{{ asset('themes/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('themes/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script> --}}

<script type="text/javascript" src="{{asset('themes/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('themes/plugins/select2/js/select2.full.min.js')}}"></script>


<!-- Select2 -->
<script src="{{asset('themes/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('themes/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('themes/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('themes/plugins/inputmask/jquery.inputmask.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('themes/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('themes/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('themes/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('themes/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- BS-Stepper -->
<script src="{{asset('themes/plugins/bs-stepper/js/bs-stepper.min.js')}}"></script>
<!-- dropzonejs -->
<script src="{{asset('themes/plugins/dropzone/min/dropzone.min.js')}}"></script>
  @stack('js')


  {{-- <script>
      window.livewire.on('userStore',()=>{
          $('createModal').modal('hide');
      })
  </script> --}}


<script type="text/javascript">
    window.livewire.on('userStore', () => {
        $('#createModal').modal('hide');
    });
</script>

</body>
</html>
