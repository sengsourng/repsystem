<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{auth()->user()->getAvatar()}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{auth()->user()->getFullName()}}</a>
      </div>
    </div>


    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{url('admin/')}}" class="nav-link {{ActiveSegment('')}}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              {{__('controls.Dashboard')}}
            </p>
          </a>
        </li>
        <li class="nav-item {{ActiveSegmentOpen('users')}}">
            <a href="{{url('admin/users')}}" class="nav-link {{ActiveSegment('users')}}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                {{trans('controls.Manage Users')}}
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">3</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{url('admin/users')}}" class="nav-link {{ActiveSegment('users')}}">
                        <i class="fas fa-user-shield nav-icon"></i>
                      <p>{{trans('controls.User List')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('admin/users')}}" class="nav-link">
                        <i class="fas fa-users-cog nav-icon"></i>
                    <p>{{trans('controls.User Roles')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="pages/layout/top-nav.html" class="nav-link">

                        <i class="fas fa-hand-holding-usd nav-icon"></i>
                        <p>{{trans('controls.Sell Comission Agents')}}</p>
                    </a>
                </li>

            </ul>

          </li>

        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              POS Sell
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li>
        <li class="nav-item {{ActiveSegmentOpen('products')}}{{ActiveSegmentOpen('brands')}}{{ActiveSegmentOpen('categories')}}{{ActiveSegmentOpen('units')}}">
          <a href="url('admin/products')" class="nav-link {{ActiveSegment('products')}}{{ActiveSegment('brands')}}{{ActiveSegment('categories')}}">
            <i class="nav-icon fa fas fa-cubes"></i>
            <p>
              {{trans('controls.Manage Products')}}
              <i class="fas fa-angle-left right"></i>

              <span class="badge badge-info right">6</span>
            </p>
          </a>
          <ul class="nav nav-treeview">

            <li class="nav-item">
              <a href="pages/layout/top-nav.html" class="nav-link">
                <i class="fas fa-check-circle nav-icon"></i>
                <p>{{trans('controls.Product List')}}</p>
              </a>
            </li>
            <li class="nav-item">
                <a href="{{url('admin/categories')}}" class="nav-link {{ActiveSegment('categories')}}">
                    <i class="fas fa-check-circle nav-icon"></i>
                  <p>{{trans('controls.Categories')}}</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{url('admin/brands')}}" class="nav-link {{ActiveSegment('brands')}}">
                    <i class="fas fa-check-circle nav-icon"></i>
                  <p>{{trans('controls.Brands')}}</p>
                </a>
            </li>
            <li class="nav-item">
            <a href="{{url('admin/units')}}" class="nav-link {{ActiveSegment('units')}}">
                    <i class="fas fa-check-circle nav-icon"></i>
                  <p>{{trans('controls.Units')}}</p>
                </a>
            </li>
            {{-- Warranties --}}

            <li class="nav-item">
                <a href="pages/layout/top-nav.html" class="nav-link">
                    <i class="fas fa-check-circle nav-icon"></i>
                  <p>{{trans('controls.Warranties')}}</p>
                </a>
            </li>

          </ul>

        </li>
{{-- Purchase --}}
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="fas fa-arrow-circle-down nav-icon"></i>
              <p>
                Purchases
                <i class="fas fa-angle-left right"></i>

                <span class="badge badge-info right">6</span>
              </p>
            </a>

            {{-- Purchases --}}
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/layout/top-nav.html" class="nav-link">
                  <i class="fas fa-check-circle nav-icon"></i>
                  <p>Purchase List</p>
                </a>
              </li>
              <li class="nav-item">
                  <a href="pages/layout/top-nav.html" class="nav-link">
                      <i class="fas fa-check-circle nav-icon"></i>
                    <p>Purchase Return</p>
                  </a>
              </li>
            </ul>



        </li>

{{-- Sell --}}
<li class="nav-item">
    <a href="#" class="nav-link">

      <i class="fas fa-arrow-circle-up nav-icon"></i>
      <p>
        Sells
        <i class="fas fa-angle-left right"></i>

        <span class="badge badge-info right">6</span>
      </p>
    </a>


    <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="pages/layout/top-nav.html" class="nav-link">
          <i class="fas fa-check-circle nav-icon"></i>
          <p>All Sales</p>
        </a>
      </li>
      <li class="nav-item">
          <a href="pages/layout/top-nav.html" class="nav-link">
              <i class="fas fa-check-circle nav-icon"></i>
            <p>Sales POS</p>
          </a>
      </li>
    </ul>



</li>

        <li class="nav-item">

            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                {{ __('Logout') }}
              </p>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>


        </li>


      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
