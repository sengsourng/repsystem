<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <style>
        .invalid-feedback{
            display: block;
        }
        .icheck{
            content: "";
            display: inline-block;
            position: absolute;
            width: 22px;
            height: 22px;
            border: 1px solid #D3CFC8;
            border-radius: 0;
            margin-left: 0px;
        }
    </style>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
      <!-- /.login-logo -->
        @yield('content')
      <!-- /.card -->
    </div>
    <!-- /.login-box -->

   <!-- Scripts -->
   <script src="{{ asset('js/app.js') }}" defer></script>

    </body>
</html>
