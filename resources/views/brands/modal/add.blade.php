<div class="container pt-3 px-4">
    <form action="{{ route('brands.store') }}" method="post">
        @csrf

        <div class="row">
            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="b_name">{{trans('controls.b_name')}}</label>
                    <input type="text" id="b_name" class="form-control" name="name" />

                </div>
            </div>

            <div class="col-md-12 mb-2 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="description">{{ __('controls.description')}}</label>
                    <textarea id="description" class="form-control" cols="30"  rows="5" name="description"></textarea>

                </div>
            </div>
        </div>
    </form>
</div>
