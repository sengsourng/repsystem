<div class="container pt-3 px-4">
    <form action="{{ route('brands.update',$brand) }}" method="post">
        @csrf
        {{ method_field('PUT') }}
        <div class="row">
            <div class="col-md-12 mb-3 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="b_name">{{trans('controls.b_name')}}</label>
                    <input type="text" id="b_name" class="form-control" name="name" value="{{$brand->name}}" />

                </div>
            </div>

            <div class="col-md-12 mb-3 gx-2">
                <div class="form-outline form-group">
                    <label class="form-label" for="description">{{ __('controls.description')}}</label>
                    <textarea id="description" class="form-control" cols="30"  rows="5" name="description">{{$brand->description}}</textarea>
                    {{-- {{!! Form::textarea($name, $value, [$options]) !!}} --}}
                    {{-- <input type="texteara" id="description" class="form-control" name="description" /> --}}


                </div>
            </div>
        </div>



    </form>
</div>
