<tr data-id="{{ $row->id}}">
    <th scope="row" style="width: 40px !important;">{{$row->id}}</th>
    <td>{{$row->name}}<br>
        <span class="text-success">
            <i class="fas fa-calculator me-1"></i> <span> {{ $row->products()->count() }} {{trans('controls.Products')}}</span>
        </span>

    </td>
    <td>
        {{$row->description}}
    </td>

    <td>
        <i class="fas fa-user me-1"></i><span> {{$row->users($row->created_by)->last_name}}</span>
        <br>
        <span class="text-success">
            <i class="fas fa-clock me-1"></i><span> {{ session()->get('locale')=='kh'?KhmerDateTime\KhmerDateTime::parse($row->created_at)->fromNow():$row->created_at->diffForHumans() }}</span>
        </span>

    </td>


    <td>
        <a class="btn btn-primary  edit_button sm" href="{{route('brands.edit',$row)}}"
                data-toggle="modal-ajax"
                data-modal-size="modal-lg"
                {{-- data-mdb-toggle="modal" --}}
                data-target="#openModal"
        ><i class="fa fa-edit left"></i> {{ trans('controls.edit')}}</a>


        <a href="javascript:deleteItem({{ $row->id }})"​​ class="btn btn-danger delete_button sm"><i class="fa fa-trash-alt left"></i> {{ trans('controls.delete')}}</a>


        <form data-toggle="formDelete"  id="frmDeleteItem-{{ $row->id }}" style="display: none" action="{{ route('brands.destroy',$row->id) }}" role="form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

    </td>
</tr>
