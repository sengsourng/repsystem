<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $business_id=Auth()->user()->business_id;
        // $data['brands'] = DB::table('brands')->get();
        $data['brands']=Brand::where('business_id','=',$business_id)
                                ->orderBy('id', 'DESC')->get();

               // dd($data['brands']);
        return view('brands.home',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return "Hello";
        if( request()->ajax()){
		    return view('brands.modal.add');
		}
        // return view('brands.modal.add');
        return view('brands.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];
        $add = Brand::create([
            'business_id'  =>  1,
            'name'          => $request->name,
            'description'    => $request->description,
            'created_by'    => auth()->user()->id
        ]);

        if ($add) {
            $response = [
                'status' => true,
                'data' => $add,
                'dom' => view('brands.rows',['row' => $add])->render(),
            ];
        }
        if(request()->ajax()){

            return $response;
        }
        // dd($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {

        if( request()->ajax()){
		    return view('brands.modal.edit',compact('brand'));
		}

        return view('brands.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Brand $brand)
    {
        $response = [
            'status' => false,
            'data' => [],
        ];
        $update = $brand->update([
            'business_id'  =>  1,
            'name'          => $request->name,
            'description'    => $request->description,
            'created_by'    => auth()->user()->id
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' => $brand,
                'dom' => view('brands.rows',['row' => $brand])->render(),
            ];
        }
        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = [
            'status' => false,
        ];
        $brand=Brand::findOrFail($id);


        if ($brand->products->count() == 0) {
            $brand->delete();
            $response = [
                'status' => true,
            ];
        }
        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('brands.index');

    }
}
