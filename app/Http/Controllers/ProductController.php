<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business_id=Auth()->user()->business_id;
        $data['products']=Product::where('business_id','=',$business_id)
                                    ->orderBy('id','desc')
                                    ->get();

        //                             return $data;
        // $data['products']=DB::select('SELECT * FROM products WHERE business_id=1');
        // return $data;

        // $product=DB::select('select name from products where id = ?', [1]);

        return view('products.home',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['products']=Product::where('business_id','=',$id)
                                    ->orderBy('id','desc')
                                    ->get();

        // $data['products']=DB::select('SELECT * FROM products WHERE business_id='. $id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $Parents=Category::where('parent_id','=',0)->get();
        // dd($category);
        if( request()->ajax()){
		    return view('products.modal.edit',compact('product','Parents'));
		}

        return view('products.edit',compact('product','Parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
