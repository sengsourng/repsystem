<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['units']=Unit::orderBy('id', 'DESC')->get();
        // $data['categories']=Category::orderBy('id', 'DESC')->get();
        // dd($data['categories']);

        return view('units.home',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // return "Hello";
         $Parents=Unit::where('parent_id','=',0)->get();
         if( request()->ajax()){
		    return view('units.modal.add',compact('Parents'));
		}
        // return view('units.modal.add');
        return view('units.add',compact('Parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $response = [
                'status' => false,
                'data' => [],
            ];


            $add = Unit::create([
                'business_id'  =>  1,
                'actual_name'   => $request->actual_name,
                'short_name'    => $request->short_name,
                'base_unit_id'     => $request->base_unit_id,
                'base_unit_multiplier'    => $request->base_unit_multiplier,
                'created_by'    => auth()->user()->id
            ]);

            // dd($add);

            if ($add) {
                $response = [
                    'status' => true,
                    //'data' => $add,
                    'dom' => view('categories.rows',['row' => $add])->render(),
                ];
            }



            if(request()->ajax()){
                return $response;
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        $Parents=Unit::where('base_unit_id','=',null)->get();
        // dd($unit);
        if( request()->ajax()){
		    return view('units.modal.edit',compact('unit','Parents'));
		}

        return view('units.edit',compact('unit','Parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Unit $unit)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $unit->id,
        ];
        $update = $unit->update([
            'business_id'  =>  1,
            'actual_name'          => $request->actual_name,
            'short_name'          => $request->short_name,
            'base_unit_id'          => $request->base_unit_id,
            // 'description'    => $request->description,
            'created_by'    => auth()->user()->id
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $unit->id],
                // 'data' => $unit,
                'dom' => view('units.rows',['row' => $unit])->render(),

            ];

        }

        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $response = [
            'status' => false,
        ];
        $unit=Unit::findOrFail($id);


        // if ($unit->products->count() == 0) {
        //     $unit->delete();
        //     $response = [
        //         'status' => true,
        //     ];
        // }

        $unit->delete();
            $response = [
                'status' => true,
            ];

        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('units.index');
    }
}
