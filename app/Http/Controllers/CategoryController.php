<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories']=Category::orderBy('id', 'DESC')->get();
        // $data['categories']=Category::orderBy('id', 'DESC')->get();
        // dd($data['categories']);

        return view('categories.home',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // return "Hello";
         $Parents=Category::where('parent_id','=',0)->get();
         if( request()->ajax()){
		    return view('categories.modal.add',compact('Parents'));
		}
        // return view('categories.modal.add');
        return view('categories.add',compact('Parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $response = [
                'status' => false,
                'data' => [],
            ];


            $add = Category::create([
                'business_id'  =>  1,
                'name'          => $request->name,
                'short_code'    => $request->short_code,
                'parent_id'     => $request->parent_id,
                'description'    => $request->description,
                'created_by'    => auth()->user()->id
            ]);

            // dd($add);

            if ($add) {
                $response = [
                    'status' => true,
                    //'data' => $add,
                    'dom' => view('categories.rows',['row' => $add])->render(),
                ];
            }



            if(request()->ajax()){
                return $response;
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $Parents=Category::where('parent_id','=',0)->get();
        // dd($category);
        if( request()->ajax()){
		    return view('categories.modal.edit',compact('category','Parents'));
		}

        return view('categories.edit',compact('category','Parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Category $category)
    {
        $response = [
            'status' => false,
            'data' => [],
            // 'id' => $category->id,
        ];
        $update = $category->update([
            'business_id'  =>  1,
            'name'          => $request->name,
            'short_code'          => $request->short_code,
            'parent_id'          => $request->parent_id,
            'description'    => $request->description,
            'created_by'    => auth()->user()->id
        ]);

        if ($update) {
            $response = [
                'status' => true,
                'data' =>['id'=> $category->id],
                // 'data' => $category,
                'dom' => view('categories.rows',['row' => $category])->render(),

            ];

        }

        if(request()->ajax()){
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $response = [
            'status' => false,
        ];
        $category=Category::findOrFail($id);


        if ($category->products->count() == 0) {
            $category->delete();
            $response = [
                'status' => true,
            ];
        }
        if (request()->ajax()) {
            return $response;
        }

        return redirect()->route('categories.index');
    }
}
