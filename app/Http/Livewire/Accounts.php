<?php

namespace App\Http\Livewire;

use App\Models\Account;
use Livewire\Component;
use App\Models\AccountType;
use Illuminate\Support\Facades\DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;

class Accounts extends Component
{

    public $model = Account::class;

    public $accounts, $name, $account_number,$business_id,$account_type_id,$note,$is_closed,$created_by;
    public $account_id;
    public $updateMode = false;

    public $account_types;
    public $amount;


    public function render()
    {

        // $business_id = session()->get('user.business_id');
        $this->business_id = auth()->user()->business_id;
        // dd($business_id);

        $this->account_types = AccountType::where('business_id', $this->business_id)
                                     ->whereNull('parent_account_type_id')
                                    //  ->with(['sub_types'])
                                     ->get();

        // dd($account_types);

        $this->accounts=Account::where('is_closed','0')
                                 ->where('business_id',$this->business_id)
                                 ->get();


        return view('livewire.accounts');
    }


    private function resetInputFields(){
        $this->business_id='';
        $this->name = '';
        $this->account_number = '';
        $this->account_type_id='';
        $this->note='';
        $this->balance='';
        $this->is_closed='';
        $this->created_by='';
    }



    public function store()
    {
        $validatedDate = $this->validate([
            'business_id' =>'required',
            'name' => 'required',
            'account_number' => 'required',
            // 'account_type_id' =>'required',
            // 'created_by'    => '',

        ]);

        $AccountData=[
            'business_id' =>$this->business_id,
            'name' =>  $this->name,
            'account_number' =>  $this->account_number,
            'account_type_id' => $this->account_type_id,
            'created_by'    => auth()->user()->id,
        ];

        // $validatedDate['created_by']=Auth::user();
// dd($AccountData);
        // Unit::create($validatedDate);
        Account::create($AccountData);

        session()->flash('message', 'Units Created Successfully.');
        $this->resetInputFields();

        $this->emit('userStore');

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $this->account_id=$id;
        $this->business_id = auth()->user()->business_id;

        $DataEdit = Account::where('id',$id)->first();
        $this->account_id = $id;
        $this->name = $DataEdit->name;
        $this->account_number = $DataEdit->account_number;
        $this->account_type_id = $DataEdit->account_type_id;

    }

    public function update()
    {
        $validatedDate = $this->validate([
            'business_id' =>'required',
            'name' => 'required',
            'account_number' => 'required',
            'account_type_id' => '',
        ]);

        if ($this->account_id) {
            $UpdateData = Account::find($this->account_id);
            $UpdateData->update([
                'business_id'       =>$this->business_id,
                'name'              =>  $this->name,
                'account_number'    =>  $this->account_number,
                'account_type_id'   => $this->account_type_id,
                'created_by'        => auth()->user()->id,
            ]);
            $this->updateMode = false;
            session()->flash('message', 'Account Updated Successfully.');
            $this->resetInputFields();

            $this->emit('userStore');

        }
    }


    public function delete($id)
    {
        if($id){
            Account::where('id',$id)->delete();
            session()->flash('message', 'Users Deleted Successfully.');
        }
    }


}
