<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\AccountType;
use Livewire\WithPagination;

class AccountTypes extends Component
{
    public $model = AccountType::class;
    public $Account_Types;
    public $account_type_id;
    public $parent_account_type_id;
    public $business_id;
    public $name;

    use WithPagination;

    public $updateMode = false;

    public function render()
    {
        $this->business_id=auth()->user()->business_id;
        $this->Account_Types=AccountType::orderBy('id','DESC')
                        // ->paginate(5);
                        ->get();
        // dd($this->AccountType);

        return view('livewire.account-types');

    }

    // public function resetInputFields()
    // {
    //     $this->name='';
    //     $this->account_type_id;

    //     $this->business_id=auth()->user()->business_id;
    // }

    // public function store()
    // {
    //     $validateData=$this->validate([
    //         'name' =>'required',
    //         'parent_account_type_id' =>'null',
    //         'business_id'   => 'required'
    //     ]);


    //     dd($validateData);
    //     AccountType::create($validateData);
    //     session()->flash('message','Account Type created successfuly!');
    //     $this->resetInputFields();

    //     $this->emit('AccountTypeAdded');
    // }






    private function resetInputFields(){
        $this->name = '';
        $this->parent_account_type_id = '';
        $this->business_id = '';
    }

    public function store()
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'parent_account_type_id' => '',
            'business_id' => 'required',
        ]);

        // $validatedDate();
        $SaveData=([
            'name' => $this->name,
            'parent_account_type_id' => $this->parent_account_type_id,
            'business_id' => $this->business_id,
        ]);

        AccountType::create($SaveData);

        session()->flash('message', 'Users Created Successfully.');

        $this->resetInputFields();

        $this->emit('userStore'); // Close model to using to jquery

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $accountType = AccountType::where('id',$id)->first();
        $this->account_type_id = $id;
        $this->name = $accountType->name;
        $this->parent_account_type_id = $accountType->parent_account_type_id;

    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();


    }

    public function update()
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'parent_account_type_id' => '',
            'business_id' => 'required',
        ]);

        if ($this->account_type_id) {
            $dataUpdate = AccountType::find($this->account_type_id);
            $dataUpdate->update([
                'name' => $this->name,
                'parent_account_type_id' => $this->parent_account_type_id,
                'business_id' => $this->business_id,
            ]);
            $this->updateMode = false;
            session()->flash('message', 'Users Updated Successfully.');
            $this->resetInputFields();

            $this->emit('userStore'); // Close model to using to jquery

        }
    }

    public function delete($id)
    {
        if($id){
            AccountType::where('id',$id)->delete();
            session()->flash('message', 'Users Deleted Successfully.');
        }
    }

}
