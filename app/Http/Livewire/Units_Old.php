<?php

namespace App\Http\Livewire;

use App\Models\Unit;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Units extends Component
{
    public $units, $actual_name, $short_name, $created_by,$business_id,$unit_id,$allow_decimal;
    public $updateMode = false;

    public function render()
    {
        $this->units = Unit::all();
        return view('livewire.units');
    }

    private function resetInputFields(){
        $this->business_id='';
        $this->actual_name = '';
        $this->short_name = '';
        $this->allow_decimal='';
        $this->created_by='';
    }

    public function store()
    {
        $validatedDate = $this->validate([
            'business_id' =>'required',
            'actual_name' => 'required',
            'short_name' => 'required',
            'allow_decimal' =>'required',
            // 'created_by'    => '',

        ]);

        $UnitData=[
            'business_id' =>$this->business_id,
            'actual_name' =>  $this->actual_name,
            'short_name' =>  $this->short_name,
            'allow_decimal' => $this->allow_decimal,
            'created_by'    => auth()->user()->id,
        ];

        // $validatedDate['created_by']=Auth::user();
// dd($UnitData);
        // Unit::create($validatedDate);
        Unit::create($UnitData);

        session()->flash('message', 'Units Created Successfully.');

        $this->resetInputFields();

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $Unit = Unit::where('id',$id)->first();
        $this->unit_id = $id;
        $this->business_id = $Unit->business_id;
        $this->actual_name = $Unit->actual_name;
        $this->short_name = $Unit->short_name;
        $this->allow_decimal=$Unit->allow_decimal;

    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();


    }

    public function update()
    {
        $validatedDate = $this->validate([
            'business_id' => 'required',
            'actual_name' => 'required',
            'short_name' => 'required',

        ]);

        if ($this->unit_id) {
            $Unit = Unit::find($this->unit_id);
            $Unit->update([
                'business_id' => $this->business_id,
                'actual_name' => $this->actual_name,
                'short_name' => $this->short_name,

            ]);
            $this->updateMode = false;
            session()->flash('message', 'Units Updated Successfully.');
            $this->resetInputFields();

        }
    }

    public function delete($id)
    {
        if($id){
            Unit::where('id',$id)->delete();
            session()->flash('message', 'Units Deleted Successfully.');
        }
    }


}
