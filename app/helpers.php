<?php

if(! function_exists('ActiveSegment')){
    function ActiveSegment($name,$segment=2,$class='active'){
        return request()->segment($segment)==$name?$class:'';
    }
}

if(! function_exists('ActiveSegmentOpen')){
    function ActiveSegmentOpen($name,$segment=2,$class='menu-open'){
        return request()->segment($segment)==$name?$class:'';
    }
}

