<?php

namespace App\Models;

use App\Models\Unit;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    protected $fillable=[
        'name','business_id','type','unit_id','sub_unit_ids','brand_id',
        'category_id','sub_category_id','sku'
    ];


    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function brands($id=null){
        return Brand::findOrFail($id);
    }

    public function product_cat($id=null){
        return Category::findOrFail($id);
    }

    public function product_unit($id=null){
        return Unit::findOrFail($id);
    }
}
