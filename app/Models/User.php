<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',

        'email',
        'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];




    public function getAvatar($var = null)
    {
        return 'https://www.gravatar.com/avatar/'. md5(strtolower( trim( auth()->user()->email)));
    }


    public function getFullName()
    {
        return auth()->user()->first_name." ". auth()->user()->last_name;
    }

    public function brands(){
        return $this->hasMany(Brand::class,'created_by','id');
    }

    public function accounts(){
        return $this->hasMany(Account::class,'created_by','id');
    }


}
