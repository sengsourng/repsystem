<?php

namespace App\Models;

use App\Models\Account;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AccountType extends Model
{
    use HasFactory;

    protected $fillable=([
        'name' ,
        'parent_account_type_id' ,
        'business_id'
    ]);


    public function accounts()
    {
        return $this->belongsTo(Account::class);
        // return $this->belongsTo(Account::class, 'id');
    }


}
