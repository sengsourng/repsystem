<?php

namespace App\Models;

// use App\Models\Account;
use App\Models\AccountType;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Account extends Model
{
    use HasFactory;


    protected $fillable=[
        'business_id',
        'name',
        'account_number',
        'account_type_id',
        'note',
        'created_by',
        'is_closed',
        'deleted_at',
        'created_by',
        'deleted_at'
    ];


    public function users($id){
        return User::findOrFail($id);
        // return $this->hasMany(User::class,'created_by','id');
    }

    public function AccountTypes($id){
        return AccountType::findOrFail($id);
    }



     public function SumAccount($type,$account_id)
    {
        return $account = Account::select(DB::raw('SUM(account_transactions.amount) As amount'))
        ->leftJoin('account_transactions', 'account_transactions.account_id', '=', 'accounts.id')
        ->where('account_transactions.type', $type )
        ->where('accounts.id',$account_id)
        // ->group_by('accounts.id')
        ->get();

        // return $account;
    }


}
