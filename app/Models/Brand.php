<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Brand extends Model
{
    use HasFactory;

    protected $fillable=[
        'business_id','name','description','created_by','deleted_at'
    ];


    public function users($id){
        return User::findOrFail($id);
        // return $this->hasMany(User::class,'created_by','id');
    }

    public function products(){
        // return $this->hasMany(Product::class);
        return $this->hasMany(Product::class, 'brand_id','id');
    }




    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = Auth::id();
            // $model->updated_by = Auth::id();
        });
        // static::updating(function ($model) {
        //     $model->updated_by = Auth::id();
        // });
    }

}
