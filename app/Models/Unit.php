<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Unit extends Model
{
    use HasFactory;

    protected $fillable=[
        'business_id',
        'actual_name',
        'short_name',
        'allow_decimal',
        'created_by'
    ];



    public function users($id){
        return User::findOrFail($id);
        // return $this->hasMany(User::class,'created_by','id');
    }




    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = Auth::id();
            // $model->updated_by = Auth::id();
        });
        static::updating(function ($model) {
            // $model->updated_by = Auth::id();
        });
    }

}
