<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    protected $appends = ['childs', 'products'];
    protected $table = 'categories';

    protected $fillable=[
        'name','short_code','description',
        'business_id','parent_id','created_by',
        'category_type'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = Auth::id();
            // $model->updated_by = Auth::id();
        });
        // static::updating(function ($model) {
        //     $model->updated_by = Auth::id();
        // });
    }


    public function products()
    {
        return $this->hasMany(Product::class, 'category_id','id');
    }
    public function CatParent($id=null)
    {
        return Category::findOrFail($id);
        // $cat=Category::WHERE('parent_id',$id);

        // return $this->hasMany(Category::class, 'parent_id',$id);

    }
}
